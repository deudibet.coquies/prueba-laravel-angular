import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PersonaServiceService {
url = "http://personasapi.test/api/prueba";
headers = new Headers({'content-type':'application/json'});
  constructor(private http:HttpClient) { }

  getPersona(){
    let url = this.url;
    return this.http.get(url);
  }

  nuevaPersona(dato : any) {
    return this.http.post(this.url, dato);
  }

  editarPersona(dato:any){
    return this.http.put(`${this.url}/${dato.id}`, dato);
  }

}
